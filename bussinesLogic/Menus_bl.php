<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Menus_bl {

  public static function getMenus($obj = false){
      return ($obj) ? Menu::toObject(Menu::getAll()) : Menu::getAll();
  }
  
  public static function orderMenus($menus){
      
      $main = [];
      
      foreach ($menus as $key => $menu) {
          if(!isset($menu["parent"])){
              $main[$menu["id"]] = $menu;
          }else{
              $main[$menu["parent"]]["children"][$menu["id"]] = $menu;
          }
      }
      
      return $main;
  }

}
