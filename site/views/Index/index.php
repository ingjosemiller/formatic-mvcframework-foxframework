<?php $this->loadModule("header"); ?>
<body>
    <header>
        
        <ul>
            <?php foreach ($this->menus as $menu) : ?>
            <li><a href="<?php echo $menu["url"]; ?>"><?php echo $menu["name"]; ?></a>
                <ul>
                    <?php foreach ($menu["children"] as $menu) : ?>
                    <li><a href="<?php echo $menu["url"]; ?>"><?php echo $menu["name"]; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <?php endforeach; ?>
        </ul>
        
    </header>
    <div class="row">
      <div class="col-7">
        <img src="<?php echo URL; ?>public/img/hp-08-10-hero-desk.jpeg" alt="">
      </div>
     <div class="col-5">
     <img src="<?php echo URL; ?>public/img/hp-08-10-hero-boot.jpeg" alt="">
     </div>
    </div>
    <div class="row miniMenu">
      <div>Algo</div>
      <div>Otro</div>
      <div>Otro algo</div>
      <div>Algo 1</div>
      <div>Algo 2</div>
      <div>Algo 3</div>
    </div>
    <div class="row">
      <div class="col-12">
        <picture class="cq-dd-image ">
          <source srcset="https://images.timberland.com/is/image/TimberlandBrand/hp-08-10-snk-desk?$SCALE-ORIGINAL$" media="(min-width: 1024px)">
          <source srcset="https://images.timberland.com/is/image/TimberlandBrand/hp-08-10-snk-desk?$SCALE-ORIGINAL$" media="(min-width: 640px)">
          <img srcset="https://images.timberland.com/is/image/TimberlandBrand/hp-08-10-snk-mob?$SCALE-ORIGINAL$" alt="WATERPROOF HEAD TO TOE">
        </picture>
      </div>
    </div>
    <div class="row">
      <div class="col-7"></div>
      <div class="col-5"></div>
    </div>
    <div class="row">
      <div class="col-12"></div>
    </div>
    <div class="row">
      <div class="col-7"></div>
    <div class="col-5"></div>
    </div>
    <div class="row">
      <div class="col-5"></div>
      <div class="col-7"></div>
    </div>
    <div class="row preFooter"></div>
    <footer class="row"></footer>
</body>
</html>

